# coding: utf-8
# frozen_string_literal: true

require 'rails_helper'

# committeeを使う宣言
describe "/<app_name>/:customer_alias/<feature>" do
  include Committee::Rails::Test::Methods

  # ここでpathの設定をする
  def committee_options
    @committee_options ||= {
      schema_path: Rails.root.join('schemas', 'api', "name-of-schema-file").to_s,
      check_content_type: false,
    }
  end

  let(:customer) { create(:customer) }
  let(:api_ver) { "1.4.2" }
  let(:auth_key) { "auth_key" }
  let(:payload) {  }
  let(:headers) {
    {
      "HTTP_API_VER"       => api_ver,
      'HTTP_AUTH_KEY'      => auth_key,
      "HTTP_DEVELOPER_KEY" => "cQuxZ2hVxNO7",
      'HTTP_APP_NAME'      => "<app_name>",
      "HTTP_APP_ID"        => "1",
      "HTTP_DEVICE_NAME"   => "device1",
      "HTTP_MAC_ADDRESS"   => "02:00:00:00:00:00",
      "HTTP_DEVICE_ID"     => "",
      "HTTP_YE_DEVICE_ID"  => "",
      "HTTP_PRODUCT"       => "iPhone",
      "HTTP_OS"            => "ios",
      "HTTP_OS_VER"        => "13.1.3",
      "HTTP_APP_VER"       => "4.0.0",
      "HTTP_LOCALIZE"      => "en-JP",
      "HTTP_DOMAIN"        => customer.alias_name,
    }
  }

  describe "Confirm Open API Schema" do
    # For method get
    it do
      get "/secured/<app_name>/#{customer.alias_name}/<feature>", headers: headers
      # /secured/docs/<yourdomain>/token/check/
      assert_request_schema_confirm
      assert_response_schema_confirm
    end

    # For method post
    it do
      post "/secured/<app_name>/#{customer.alias_name}/<feature>", params: payload, headers: headers
      assert_request_schema_confirm
      assert_response_schema_confirm
    end

    # For method put
    it do
      put "/secured/<app_name>/#{customer.alias_name}/<feature>", params: payload, headers: headers
      assert_request_schema_confirm
      assert_response_schema_confirm
    end
    
  end
end
  